extends Area2D

onready var x = preload("res://assets/x.png")
onready var o = preload("res://assets/o.png")

var selected = false
export var pos = - 1

func _ready():
	$mouse_over.hide()

func _on_POS_mouse_entered():
	if(!selected and !Game.win):
		$mouse_over.show()

func _on_POS_mouse_exited():
	$mouse_over.hide()

func play_x():
	if(!selected and !Game.win):
		$x_o.set_texture(x)
		Game.data_store[pos] = 'x'
		Game.check_win(pos, 'x')

func play_o():
	if(!selected and !Game.win):
		$x_o.set_texture(o)
		Game.data_store[pos] = 'o'
		Game.check_win(pos, 'o')

func _on_POS_input_event(viewport, event, shape_idx):
	# Event is triggered twice: Once for mouse button down and once
	# for mouse button up.
	if(event is InputEventMouseButton and event.pressed):
		if(event.button_index == BUTTON_LEFT):
			play_x()
			$mouse_over.hide()
			selected = true
			Game.play_computer()
#		else:
#			# This is for testing purposes or if you want to play with two players.
#			# Enable this for 2 players
#			play_o()
#			$mouse_over.hide()
#			selected = true
			
			
